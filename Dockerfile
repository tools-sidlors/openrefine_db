#FROM java:openjdk-8u111-alpine
FROM adoptopenjdk/openjdk11:jdk-11.0.11_9-alpine-slim
ARG DOCKER_USER=sidlors
RUN addgroup -S $DOCKER_USER && adduser -S $DOCKER_USER -G $DOCKER_USER
RUN apk add --no-cache bash && apk add --no-cache curl
USER sidlors
WORKDIR /home/sidlors/openrefine
COPY ./openrefine/ /home/sidlors/openrefine/
EXPOSE 3333
#ENTRYPOINT [ "/bin/bash" ]
CMD ./refine -i 0.0.0.0 -p 3333